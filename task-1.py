# -*- coding: utf-8 -*-
"""
Created on Fri May 26 20:18:09 2017

@author: dipak
"""
import re

freq = {}
text = open('test.txt', 'r')
string = text.read().replace(',','').replace('\'','').replace('.','').lower()
string = re.sub(r"(\b|\s+\-?|^\-?)(\d+|\d*\.\d+)\b", " ", string)
pattern = re.findall(r'\b[a-z]{2,15}\b', string)
 
for word in pattern:
    count = freq.get(word,0)
    freq[word] = count + 1
     
freq_list = freq.keys()

print('{:10}{:15}{:3}'.format('Number','Word','Count'))
print('-' * 30)
i=1;
for words in freq_list:
    print('{}        {:15} {:3}'.format(i, words, freq[words]))
    i=i+1
