# -*- coding: utf-8 -*-
"""
Created on Fri May 26 22:25:50 2017

@author: dipak
"""

#A = [1,3,-4,1,-2,3,1,6,8,1,-4,2,1,60,90]
A = raw_input().split(' ')   # Space separated integer values
A = [int(num) for num in A]

n = len(A)
count=1
for i in range(n):
    for j in range(i+1,n):
        if A[i] == A[j]:
            count+=1
            print "("+str(i)+","+str(j)+")"
            
print "The number of duplicate index pairs are : "+str(count)